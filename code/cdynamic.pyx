# -*- coding: utf-8 -*-

"""
dynamic
=======

Provides the functions required to integrate the full PDE describing the
dynamics of the Phi^4 theory on the wormhole spacetime. The PDE will be solved
by the method of lines in terms of the function Phi=F[0] and two auxilliary
variables Psi=F[1] and Pi=F[2]. Note that the choice of independent variables
is such that y represents space and s represents time.

The module requires numpy.

Functions
---------
update_spatial_derivative(F, dFdy, h)
    Calculate and return the gradient of F wrt y based on the current values of
    F at an array of values of y, using the method of finite differences.
update_time_derivative(F, dFdy, dFds, Y, a, h)
    Calculate and return the gradient of F wrt s based on the current values of
    F and dFdy at an array of values of y, based on the full dynamical PDE.
rk_step(F, dFdy, Y, a, h, step, f_intermediate, fdot1, fdot2, fdot3,
        fdot4)
    Advance the values in F by one time step according to the fourth order
    Runge-Kutta method, and return the advanced values.
    
"""

import numpy as np
cimport cython
cimport numpy as np

@cython.boundscheck(False)
@cython.wraparound(False)  
cpdef np.ndarray[np.float64_t, ndim=2] update_spatial_derivative(
        np.ndarray[np.float64_t, ndim=2] F,
        np.ndarray[np.float64_t, ndim=2] dFdy,
        np.float64_t h,
        np.int_t pts):
    """
    Use fourth order finite differences to calculate the gradient of F with
    respect to y, given the current values of F at an array of points in the
    spatial domain.
    
    Arguments:
    ---------
    F: numpy array with three columns
        the value of Phi, Psi and Pi at each point y in the spatial domain
    dFdy: numpy array with three columns
        the gradient wrt y of Phi, Psi and Pi at each point y in the spatial
        domain
    h: float
        the distance between points in the spatial domain
        
    Returns:
    -------
    dFdy: numpy array with three columns
        the gradient wrt y of Phi, Psi and Pi at each point y in the spatial
        domain
    
    """
    cdef np.ndarray[np.float64_t, ndim=2] Fp1 = np.append(F[1:,:], np.zeros((1,3)), 0)
    cdef np.ndarray[np.float64_t, ndim=2] Fp2 = np.append(F[2:,:], np.zeros((2,3)), 0)
    cdef np.ndarray[np.float64_t, ndim=2] Fm1 = np.append(np.zeros((1,3)), F[:(pts-1),:], 0)
    cdef np.ndarray[np.float64_t, ndim=2] Fm2 = np.append(np.zeros((2,3)), F[:(pts-2),:], 0)
    dFdy = (Fm2 - 8*Fm1 + 8*Fp1 - Fp2)/(12*h)
    dFdy[0,:] = (- 25*F[0,:] + 48*F[1,:] - 36*F[2,:]
                         + 16*F[3,:] - 3*F[4,:])/(12*h)
    dFdy[1,:] = (- 3*F[0,:] - 10*F[1,:] + 18*F[2,:]
                         - 6*F[3,:] + F[4,:])/(12*h)
    dFdy[pts-2,:] = (3*F[pts-1,:] + 10*F[pts-2,:] - 18*F[pts-3,:]
                          + 6*F[pts-4,:] - F[pts-5,:])/(12*h)
    dFdy[pts-1,:] = (25*F[pts-1,:] - 48*F[pts-2,:] + 36*F[pts-3,:]
                          - 16*F[pts-4,:] + 3*F[pts-5,:])/(12*h)
    return dFdy
    
    


@cython.boundscheck(False)
@cython.wraparound(False)  
cpdef np.ndarray[np.float64_t, ndim=2] update_time_derivative(
        np.ndarray[np.float64_t, ndim=2] F,
        np.ndarray[np.float64_t, ndim=2] dFdy,
        np.ndarray[np.float64_t, ndim=2] dFds,
        np.ndarray[np.float64_t, ndim=2] alpha,
        np.float64_t a,
        np.float64_t h,
        np.int_t pts):
    """
    Return the derivative of F with respect to y based on current values of F
    using the PDE.
    
    Arguments:
    ---------
    F: numpy array with three columns
        the value of Phi, Psi and Pi at each point y in the spatial domain
    dFdy: numpy array with three columns
        the gradient wrt y of Phi, Psi and Pi at each point y in the spatial
        domain
    dFds: numpy array with three columns
        the gradient wrt s of Phi, Psi and Pi at each point y in the spatial 
        domain
    Y: numpy vector
        the vector of points y in the spatial domain
    a: float
        the wormhole radius
        
    Returns:
    -------
    dFds: numpy array with three columns
        the gradient wrt s of Phi, Psi and Pi at each point y in the spatial 
        domain
        
    """
    dFdy = update_spatial_derivative(F, dFdy, h, pts)
    dFds[:,0] = F[:,2] + alpha[:,0]*F[:,1]
    dFds[:,1] = (dFdy[:,2] + alpha[:,1]*F[:,1] + alpha[:,0]*dFdy[:,1]
                 - 0.1*(F[:,1] - dFdy[:,0]))
    dFds[:,2] = (dFdy[:,1] + alpha[:,1]*F[:,2] + alpha[:,0]*dFdy[:,2]
                 + alpha[:,3]*(F[:,1] + alpha[:,0]*F[:,2])
                 + 2*alpha[:,2]*F[:,0]*(1-F[:,0]*F[:,0]))
    
    # Kreiss-Oliger dissipation
    cdef np.ndarray[np.float64_t, ndim=2] Fp1 = np.append(F[1:,:], np.zeros((1,3)), 0)
    cdef np.ndarray[np.float64_t, ndim=2] Fp2 = np.append(F[2:,:], np.zeros((2,3)), 0)
    cdef np.ndarray[np.float64_t, ndim=2] Fp3 = np.append(F[3:,:], np.zeros((3,3)), 0)
    cdef np.ndarray[np.float64_t, ndim=2] Fm1 = np.append(np.zeros((1,3)), F[:-1,:], 0)
    cdef np.ndarray[np.float64_t, ndim=2] Fm2 = np.append(np.zeros((2,3)), F[:-2,:], 0)
    cdef np.ndarray[np.float64_t, ndim=2] Fm3 = np.append(np.zeros((3,3)), F[:-3,:], 0)
    dFds[:,2] = dFds[:,2] + (Fm3[:,2] + Fp3[:,2] - 6*(Fm2[:,2] + Fp2[:,2])
                        + 15*(Fm1[:,2] + Fp1[:,2]) - 20*(F[:,2]))
    dFds[0,:] = 0
    dFds[pts-1,:] = 0
    #for y_i in range(3, F[:,0].size-3):
    #    dFds[y_i,2] += (F[y_i-3,2] + F[y_i+3,2] - 6*(F[y_i-2,2] + F[y_i+2,2])
     #                   + 15*(F[y_i-1,2] + F[y_i+1,2]) - 20*(F[y_i,2]))
    
    return dFds

@cython.boundscheck(False)
@cython.wraparound(False)   
cpdef np.ndarray[np.float64_t, ndim=2] rk_step(np.ndarray[np.float64_t, ndim=2] F,
            np.ndarray[np.float64_t, ndim=2] dFdy,
            np.ndarray[np.float64_t, ndim=2] alpha,
            np.float64_t a,
            np.float64_t h,
            np.float64_t step,
            np.ndarray[np.float64_t, ndim=2] F_intermediate,
            np.ndarray[np.float64_t, ndim=2] Fdot1,
            np.ndarray[np.float64_t, ndim=2] Fdot2,
            np.ndarray[np.float64_t, ndim=2] Fdot3,
            np.ndarray[np.float64_t, ndim=2] Fdot4,
            np.int_t pts):
    """
    Return the values in F advanced by one time step according to the fourth
    order Runge-Kutta method.
    
    Arguments:
    ---------
    F: numpy array with three columns
        the value of Phi, Psi and Pi at each point y in the spatial domain
    dFdy: numpy array with three columns
        the gradient wrt y of Phi, Psi and Pi at each point y in the spatial
        domain
    Y: numpy vector
        the vectoy of points y in the spatial domain
    a: float
        the wormhole radius
    h: float
        the distance between points in the spatial domain
    step: float
        the size of each time step
    F_intermediate: numpy array with three columns
        estimate of F at time (s + step/2) based on intermediate gradient
        estimate Fdot1, then Fdot2, then Fdot3
    Fdot1: numpy array with three columns
        estimate of dFds at time s based on F at time s
    Fdot2: numpy array with three columns
        estimate of dFds at time (s + step/2) based on Fdot1 and F at time s
    Fdot3: numpy array with three columns
        estimate of dFds at time (s + step/2) based on Fdot2 and F at time s
    Fdot4: numpy array with three columns
        estimate of dFds at time (s + step) based on Fdot3 and F at time s

    """          
    Fdot1 = update_time_derivative(F, dFdy, Fdot1, alpha, a, h, pts)
    F_intermediate = F + Fdot1*step/2
    
    Fdot2 = update_time_derivative(F_intermediate, dFdy, Fdot2, alpha, a, h, pts)
    F_intermediate = F + Fdot2*step/2
    
    Fdot3 = update_time_derivative(F_intermediate, dFdy, Fdot3, alpha, a, h, pts)
    F_intermediate = F + Fdot3*step    
    
    Fdot4 = update_time_derivative(F_intermediate, dFdy, Fdot4, alpha, a, h, pts)
    return F + (Fdot1 + Fdot2*2 + Fdot3*2 + Fdot4)*step/6
    
