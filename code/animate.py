# -*- coding: utf-8 -*-
"""
Created on Thu Jun  6 13:49:31 2019

@author: aw592
"""

import numpy as np
from scipy.fftpack import fft
import matplotlib.pyplot as plt
from static import static as st
from matplotlib import animation
import time
import dynamic2 as dyn

def update_time_derivative(F, dFdy, dFds, alpha, a, h):
    """
    Return the derivative of F with respect to y based on current values of F
    using the PDE.
    
    Arguments:
    ---------
    F: numpy array with three columns
        the value of Phi, Psi and Pi at each point y in the spatial domain
    dFdy: numpy array with three columns
        the gradient wrt y of Phi, Psi and Pi at each point y in the spatial
        domain
    dFds: numpy array with three columns
        the gradient wrt s of Phi, Psi and Pi at each point y in the spatial 
        domain
    Y: numpy vector
        the vector of points y in the spatial domain
    a: float
        the wormhole radius
        
    Returns:
    -------
    dFds: numpy array with three columns
        the gradient wrt s of Phi, Psi and Pi at each point y in the spatial 
        domain
        
    """
    Fp1 = np.append(F[1:,:], np.zeros((1,3)), 0)
    Fp2 = np.append(F[2:,:], np.zeros((2,3)), 0)
    Fp3 = np.append(F[3:,:], np.zeros((3,3)), 0)
    Fm1 = np.append(np.zeros((1,3)), F[:-1,:], 0)
    Fm2 = np.append(np.zeros((2,3)), F[:-2,:], 0)
    Fm3 = np.append(np.zeros((3,3)), F[:-3,:], 0)
    
    dFdy = (Fm2 - 8*Fm1 + 8*Fp1 - Fp2)/(12*h)
    dFdy[0,:] = (- 25*F[0,:] + 48*F[1,:] - 36*F[2,:]
                         + 16*F[3,:] - 3*F[4,:])/(12*h)
    dFdy[1,:] = (- 3*F[0,:] - 10*F[1,:] + 18*F[2,:]
                         - 6*F[3,:] + F[4,:])/(12*h)
    dFdy[-2,:] = (3*F[-1,:] + 10*F[-2,:] - 18*F[-3,:]
                          + 6*F[-4,:] - F[-5,:])/(12*h)
    dFdy[-1,:] = (25*F[-1,:] - 48*F[-2,:] + 36*F[-3,:]
                          - 16*F[-4,:] + 3*F[-5,:])/(12*h)
    #dFdy = update_spatial_derivative(F, dFdy, h)
    dFds[:,0] = F[:,2] + alpha[:,0]*F[:,1]
    dFds[:,1] = (dFdy[:,2] + alpha[:,1]*F[:,1] + alpha[:,0]*dFdy[:,1]
                 - 0.1*(F[:,1] - dFdy[:,0]))
    dFds[:,2] = (dFdy[:,1] + alpha[:,1]*F[:,2] + alpha[:,0]*dFdy[:,2]
                 + alpha[:,3]*(F[:,1] + alpha[:,0]*F[:,2])
                 + 2*alpha[:,2]*F[:,0]*(1-F[:,0]*F[:,0]))
    
    
    # Kreiss-Oliger dissipation
    #for y_i in range(3, F[:,0].size-3):
    #    dFds[y_i,2] += (F[y_i-3,2] + F[y_i+3,2] - 6*(F[y_i-2,2] + F[y_i+2,2])
    #                    + 15*(F[y_i-1,2] + F[y_i+1,2]) - 20*(F[y_i,2]))
    
    dFds[:,2] += (Fm3[:,2] + Fp3[:,2] - 6*(Fm2[:,2] + Fp2[:,2])
                        + 15*(Fm1[:,2] + Fp1[:,2]) - 20*(F[:,2]))
    dFds[0,:] = 0
    dFds[-1,:] = 0
    
    
    return dFds




start = time.time()

    
a = 0.5
h = 0.002
max_time = 100.0
ic = [2.0,0.0]
space_points = 2*int(np.floor(np.pi/(2.0*h))) + 1
middle = int(np.floor(space_points/2))
Y = np.linspace(-np.pi/2, np.pi/2, space_points)
alpha = np.zeros((space_points, 4))
alpha[:,0] = - np.sin(Y)
alpha[:,1] = - np.cos(Y)
alpha[:,2] = a*a/(np.cos(Y)*np.cos(Y))
alpha[:,3] = 2*np.tan(Y)

r = a*np.tan(Y)
r = r[middle:-1]
for i in range(r.size):
    if r[i] > 8.0:
        r = r[0:i-1]
        break        
ig, static_kink = st.find_initial_grad(a, r, returnphi=True)
ic_pos = np.append(static_kink, np.ones(int(np.ceil(space_points/2))+1 - r.size))
initial_condition = np.append(-np.flipud(ic_pos)[0:-1], ic_pos)

(E, bound_state_r) = st.find_bound_state_energy(a, r, return_bound_state=True)
rescaled = bound_state_r*(r**2 + a**2)**(-1.0/2.0)
bound_state_ext = np.append(rescaled, np.zeros(int(np.ceil(space_points/2))+1 - r.size))
bound_state = np.append(np.flipud(bound_state_ext)[0:-1], bound_state_ext)

F = np.zeros((Y.size, 3))
F0 = initial_condition + ic[0]*np.exp(-np.tan(Y)**2)
F[:,0] = F0
dFdy = np.zeros(F.shape)
dFdy = dyn.update_spatial_derivative(F, dFdy, h)
F[:,1] = dFdy[:,0]
F[:,2] = dFdy[:,0]*np.sin(Y)

F_intermediate = np.zeros(F.shape)
Fdot1 = np.zeros(F.shape)
Fdot2 = np.zeros(F.shape)
Fdot3 = np.zeros(F.shape)
Fdot4 = np.zeros(F.shape)
step = h/2.0
peaks = np.array([F0[middle]])
peak_times = np.array([0.0])

time_points = int(np.floor(max_time/step))

s = np.linspace(0, max_time, time_points)
Phi = np.zeros((space_points,(time_points/10),3))
dPhi = np.zeros((time_points/10,3))
for s_index in range(time_points):
    Pi0 = F[middle,2]
    
    Fdot1 = update_time_derivative(F, dFdy, Fdot1, alpha, a, h)
    F_intermediate = F + Fdot1*step/2
    
    Fdot2 = update_time_derivative(F_intermediate, dFdy, Fdot2, alpha, a, h)
    F_intermediate = F + Fdot2*step/2
    
    Fdot3 = update_time_derivative(F_intermediate, dFdy, Fdot3, alpha, a, h)
    F_intermediate = F + Fdot3*step    
    
    Fdot4 = update_time_derivative(F_intermediate, dFdy, Fdot4, alpha, a, h)
    
    F += (Fdot1 + Fdot2*2 + Fdot3*2 + Fdot4)*step/6
    if s_index % 1000 == 0: print(s_index*step)
    if s_index % 10 == 0:
        Phi[:,s_index/10,:] = F
        dPhi[s_index/10,:] = dyn.update_spatial_derivative(F, dFdy, h)[middle,:]
    if ((F[middle,2]*Pi0 < 0)):
        peaks = np.append(peaks, np.abs(F[middle,0]))
        peak_times = np.append(peak_times, s[s_index])



        
plt.figure(0)
plt.plot(Y, F0,'b')
plt.plot(Y, F[:,0],'r')
plt.plot(Y, initial_condition, 'g')

plt.figure(1)
plt.plot(np.linspace(0, max_time, time_points/10), dPhi[:,0]-Phi[middle,:,1])
#plt.plot(np.linspace(0, max_time, time_points), Phi[:,1], 'r')

plt.figure(2)
plt.plot(np.linspace(0, max_time, time_points/10), np.abs(Phi[middle,:,0]))
#plt.savefig('oscillating_decay.pdf')

plt.figure(3)
plt.loglog(peak_times, peaks, 'b')
plt.loglog(peak_times, 4.1*peak_times**(-1.0/2.0), 'k')


#peak_data = np.transpose(np.array([peak_times, peaks]))
#peak_filename = "out/peaks_even{}_{}_{}_{}.npy".format(str(ic[0]), str(a), str(h), str(max_time))
#np.save(peak_filename, peak_data)


plt.figure(4)
ft = fft(Phi[middle,:,0])
plt.plot(np.linspace(0, time_points/10, time_points/10)*2*np.pi/(a*time_points*step),
         np.abs(ft), 'r')
plt.axvline(x=np.sqrt(st.find_bound_state_energy(a)), color='k')
plt.axis([0.0, 6.0, 0.0, 5000.0])



#%%

fig = plt.figure(5)
ax = plt.axes(xlim=(-np.pi/2,np.pi/2),ylim=(-ic[0],ic[0]))
line, = ax.plot([],[],'b',lw=2)
static, = ax.plot([],[],'r',lw=2)


def animate(j):
    line.set_data(Y, Phi[:,j,0] - initial_condition)
    static.set_data(Y, ic[0]*a*bound_state)
    return line,
    
anim = animation.FuncAnimation(fig, animate, frames=min(5000, time_points/10),
                               interval=1)

anim.save('animation.mp4', fps=250, bitrate=1000)

end = time.time()
print("Time = " + str(end-start) + " seconds")
print("Time = " + str((end-start)/60) + " minutes")