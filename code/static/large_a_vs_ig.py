# -*- coding: utf-8 -*-
"""
This script plots the square of the initial gradient of the kink against the
inverse square of the wormhole radius. It is designed for checking the
relationship between these quantities for large wormhole radii.

It uses numpy, matplotlib and the local module static.py.

"""

import numpy as np

import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerLine2D
from matplotlib.ticker import ScalarFormatter

import static as st

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']


r = np.linspace(0, 8.0, 10000)  # Initialise domain.
# Create arrays to contain the values of 1/a^2 and of the initial gradient
epsilon_array = np.linspace(0.0000001, 0.001, 20)
ig_array = []

# For each value of 1/a^2, find the initial gradient for the corresponding a.
for epsilon in epsilon_array:
    a = 1/np.sqrt(epsilon)
    ig = st.find_initial_grad(a, r)
    ig_array.append(ig**2)  # Add the square of this initial gradient to array.
       
# Plot the resulting squares of the initial gradients against 1/a^2
plt.figure(1)
red_dots, = plt.plot(epsilon_array, ig_array, 'ro',label='numerical')
# Add the predicted square of the initial gradient as a function of 1/a^2.
x = np.linspace(0, 0.001, 10)
blue_line, = plt.plot(x, 1 + (8*np.log(2) - 2)*x/3, 'b',
                      label='predicted: $\mathcal{E}=2\epsilon^2$')
# Remove axis offset, label the axes and add the legend.
ax = plt.gca()
ax.yaxis.set_major_formatter(ScalarFormatter(useOffset=False))
plt.xlabel('$\epsilon^2$')
plt.ylabel('$(\Phi^\prime(0))^2$')
plt.legend(handler_map={red_dots: HandlerLine2D(numpoints=1)},loc=2)











