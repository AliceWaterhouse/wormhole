# -*- coding: utf-8 -*-
"""
This script estimates the critical value of the wormhole radius when the even
internal mode of the kink disappears into the continuous spectrum.

By default, it uses the fact that this is the wormhole radius where the
eigenstate at the edge of the continuous spectrum, which has energy 4, goes
from having one zero to having no zeros. In this context, "eigenstate" means
eigenstate of the quantum mechanics problem associated with linear stability of
the kink.

It can also use the fact that the integral of the QM potential should vanish at
the critical wormhole radius. The method used depends on the value of
use_eigenstate set in line 27.

It requires numpy, scipy, matplotlib and the local module static.py.

"""

import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

import static as st

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']
    

use_eigenstate = False  # Use the eigenstate method.

r = np.linspace(0, 7.5, 10000)  # Initialise the domain.

# Define the range of wormhole radii to consider and pick the first one to try.
amin = 0.29
amax = 0.4
a = (amin + amax)/2

# Find the initial gradient for this wormhole radius, generate the
# corresponding eigenstate or integral, and extract the value at the edge of
# the domain.
ig = st.find_initial_grad(a, r) 
if use_eigenstate: v = odeint(st.schrodinger, [0,ig,1,0], r, (a,4))
else: v = odeint(st.int_potential, [0,ig,0], r, (a,))
delta = v[-1,2]

# Use a bisection method to find the wormhole radius where the eigenstate or
# integral is zero at the edge of the domain.
while np.absolute(delta) > 0.00001:
    if delta>0: amin = a  # Current wormhole radius too small.
    elif delta<0: amax = a  # Current wormhole radius too big.
    # Update a, the initial gradient and the value of the corresponding
    # eigenstate or integral at the edge of the domain.
    a = (amin + amax)/2
    ig = st.find_initial_grad(a, r)
    if use_eigenstate: v = odeint(st.schrodinger, [0,ig,1,0], r, (a,4))
    else: v = odeint(st.int_potential, [0,ig,0], r, (a,))
    delta = v[-1,2]
    # Print to track the progress of a.
    print('a = '+str(a)+', delta = '+str(delta))

# Define the QM potential for this critical value of a.
W = a**2/(r**2 + a**2)**2 - 2*(1 - 3*v[:,0]**2) 
 
# Plot the (shifted) QM potential and its eigenstate or integral. 
plt.plot(r, v[:,2], 'b')
plt.plot(r, W-4, 'r')
# Add a dotted line to guide the eye.
plt.axhline(y=0, color='k',dashes=[10, 5, 10, 5])

# Calculate and print the integral of the potential for this critical value of
# a. Note that if the integral method was used, this will automatically be
# very small.
I = odeint(st.int_potential, [0,ig,0], r, (a,))
print('I = ' + str(I[-1,2]))
print('I = ' + str(np.sum(W-4)*7.5/10000.0))
















