# -*- coding: utf-8 -*-

"""
This script finds the kink for the wormhole radius specified below. It uses the
function find_initial_grad from the module static.py to generate the required
initial gradient and the kink solution for positive values of r, then plots the
kink for both positive and negative radii using the odd parity property. The
initial gradient is given in the legend.

It requires numpy and matplotlib in addition to the static.py and
plotting_functions.py modules.

"""

import numpy as np
import matplotlib.pyplot as plt

import static as st
import plotting_functions as pf

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']

r = np.linspace(0, 8, 10000)  # Initialise domain.
a = 1.0                       # Assign wormhole radius.

# Generate the kink for this wormhole radius.
(ig, phi) = st.find_initial_grad(a=a, r=r, returnphi=True)

# Create a figure with the y axis in the centre of the plot.
fig = plt.figure(0)
ax = fig.add_subplot(1, 1, 1)
pf.put_axes_at_zero(ax)

# Plot the kink against the r array, giving the initial gradient in the legend.
plt.figure(0)
plt.plot(r, phi[:], 'b')
plt.plot(-r, -phi[:], 'b')
plt.legend(['$\Phi^\prime(0) =$ ' + "{:.6f}".format(ig)], loc=4)
# Add axis labels in a sensible position.
plt.xlabel('$r$', fontsize=14)
plt.ylabel('$\Phi(r)$', fontsize=14, rotation=0)
ax.xaxis.set_label_coords(0.97, 0.45)
ax.yaxis.set_label_coords(0.38, 0.95)
# Remove the zero ticks and save.
pf.remove_centre_ticks(ax)
plt.savefig('soliton.pdf')
