# -*- coding: utf-8 -*-
"""
This script plots the order a^(-2) perturbation to the kink on the real line,
which is relevant in the large a limit.

It requires numpy, scipy, matplotlib and the local module plotting_functions.

"""

import numpy as np
from scipy.special import spence
from matplotlib import pyplot as plt

import plotting_functions as pf

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']


r = np.linspace(-8, 8, 1000)  # Domain in which to define perturbation.

# Define the three functions from which the perturbation is built.
def f_1(r):
    return r*(3 - 8*np.cosh(2*r) - np.cosh(4*r))
    
def f_2(r):
    f2 = (np.sinh(2*r) * (8*np.log(2*np.cosh(r))-1)
          + np.sinh(4*r) * np.log(2*np.cosh(r)))
    return f2
    
def f_3(r):
    return np.pi**2/2 + 6*r**2 + 6*spence(1 + np.e**(-2*r))
    
def phi_1(r):
    return (f_1(r)+f_2(r)+f_3(r)) / (24*(np.cosh(r))**2)
    
# Centre the axes at (0,0) and plot the perturbation.
fig = plt.figure(0)
ax = fig.add_subplot(1, 1, 1)
pf.put_axes_at_zero(ax)
plt.plot(r, phi_1(r))
# Add axis labels in a sensible position and set axis limits.
plt.axis([-5, 5, -0.3, 0.3])
plt.xlabel('$r$', fontsize=14)
plt.ylabel('$\Phi_1(r)$', rotation=0, fontsize=14)
ax.xaxis.set_label_coords(0.97, 0.45)
ax.yaxis.set_label_coords(0.38, 0.95)
# Remove the zero ticks and save.
pf.remove_centre_ticks(ax)
plt.savefig('Phi_1.pdf')      

