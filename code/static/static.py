# -*- coding: utf-8 -*-

"""
static
======

Provides several functions for studying the static Phi^4 kink on a wormhole
spacetime. It requires numpy and scipy.

In the decsriptions below, the phrase 'phi vector' refers to a list
of two floats, the first being the value of Phi and the second being the value
of its gradient. The 'extended phi vector' includes these but has additional
entries containing the value and gradient of corresponding solutions to the
Schrodinger problem associated with stability of the kink. The 'i vector'
includes Phi, its derivative and the integral from r=0 of the potential in this
Schrodinger problem.

Functions
---------
static_model
    Return the gradient of the phi vector at some r given its value.
find_initial_grad
    Return the gradient of the kink at r=0 given a.
schrodinger
    Return the gradient of the extended phi vector at some r given its value.
find_bound_state_energy
    Given the wormhole radius, find the even or odd bound state energy of the
    Schrodinger problem corresponding to linear stability of the kink.
int_potential
    Return the gradient of the i vector at some r given its value.
    
"""

import numpy as np
from scipy.integrate import odeint


def static_model(phi, r, a):
    """
    Calculate the gradient of the phi vector at some radius r according to
    equation (2.1).
    
    Arguments:
    ---------
    phi: list of two floats
        the value of the field Phi and its gradient
    r: float
        the radius at which the phi vector takes this value
    a: float
        the wormhole radius
    
    Returns:
    -------
    list of two floats
        the gradient of the Phi vector at radius r
        
    """   
    # Extract Phi and its gradient from phi vector.    
    u = phi[0]
    v = phi[1]
    
    # Calculate and return the gradient of each component according to the
    # static ODE.
    dudr = v
    dvdr = -2*u*(1-u*u) - 2*r*v/(r*r+a*a)
    return [dudr, dvdr]


    
def find_initial_grad(a, r=None, ig_range=None, returnphi=False):
    """
    Use a shooting method to determine the required gradient of Phi at r=0 to
    just reach Phi=1 at the edge of the domain.
    
    Arguments:
    ---------
    a: float
        the wormhole radius
    r (optional): numpy array
        the domain to feed to odeint; defaults to np.linspace(0, 8, 10000)
    ig_range (optional): list of two floats
        the range of initial gradients to consider - if the required gradient
        is not within this range the function will fail; defaults to [0.0, 3.1]
    returnphi (optional): bool
        whether or not to return the final numpy array of phi values
        corresponding to the kink along with the initial gradient
        
    Returns:
    -------
    float
        the required gradient of Phi at r=0 to just reach Phi=1 at the edge of
        the domain
    (optional) numpy array of shape (2,steps)
        if returnphi is set to True, the solution with the required gradient at
        r=0 is also returned
    
    """
    # Define integration range and set the active range of initial
    # gradients to consider.
    if r is None: r = np.linspace(0, 8, 10000)           
    if ig_range is None: ig_range = [0.0, 3.1]                
    ig = (ig_range[0] + ig_range[1])/2  # First initial gradient to try
    phi = odeint(static_model, [0,ig], r, args=(a,))
    
    # Extract maximum of Phi and minimum of its gradient from phi vector.
    max_phi = np.amax(phi[:,0])
    min_grad = np.amin(phi[:,1])
    
    # Employ a bisection method to find the required initial gradient.
    while (max_phi > 1 or np.isnan(phi).any() or min_grad < 0):     
        
        if (max_phi > 1 or np.isnan(phi).any()):  # Current value of ig too big
            ig_range[1] = ig  # Only consider smaller ig in future. 
        elif min_grad < 0:    # Current value of ig too small.
            ig_range[0] = ig  # Only consider larger ig in future.
            
        # Try a new initial gradient and update max_phi and min_grad.
        ig = (ig_range[0] + ig_range[1])/2
        phi = odeint(static_model, [0, ig], r, args=(a,))
        max_phi = np.amax(phi[:,0])
        min_grad = np.amin(phi[:,1])
            
    # Return final value of initial gradient with phi array if requested.
    if returnphi:
        return(ig, phi[:,0])
    else:
        return(ig)
        

def schrodinger(v, r, a, E):
    """
    Calculate the gradient of the extended phi vector at some radius r
    according to equations (2.1) and (2.2).
    
    Arguments:
    ---------
    v: list of four floats
        the extended phi vector, containing the value of the field Phi, its
        gradient, the value of the solution to the associated Schrodinger
        equation and its gradient
    r: float
        the radius at which the phi vector takes this value
    a: float
        the wormhole radius
    E: float
        the energy of the solution to the the associated quantum mechanics
        problem
    
    Returns:
    -------
    list of four floats
        the gradient of the extended phi vector at radius r
        
    """
    # Extract the entries of the extended phi vector.
    u = v[0]  # The value of the field Phi.
    w = v[1]  # The gradient of the field Phi.
    x = v[2]  # The value of the solution to the associated QM problem.
    y = v[3]  # The gradient of the solution to the associated QM problem.
    
    #Calculate the potential of the associated QM problem at this value of r.    
    V = a*a/(r*r + a*a)**2 - 2*(1 - 3*u*u)
    # Calculate and return the gradient of each component according to
    # equations (2.1) and (2.2).
    dudr = w
    dwdr = -2*u*(1-u*u) - 2*r*w/(r*r+a*a)
    dxdr = y
    dydr = -E*x + V*x
    return [dudr, dwdr, dxdr, dydr]
    
    
def find_bound_state_energy(a, r=None, even=True, return_bound_state=False):
    """
    Use a shooting method to determine the required energy in (2.2) so that the
    solution goes to zero at the edge of the domain.
    
    Arguments:
    ---------
    a: float
        the wormhole radius
    r (optional): numpy array of floats
        the points in the domain to feed to odeint; defaults to
        np.linspace(0, 8, 10000)
    even: bool
        if even is set to True, the function will search for even bound states;
        if even is set to False, the function will search for odd bound states
    return_bound_state (optional): bool
        whether or not to return the final numpy array containing the solution
        to the QM problem which goes to zero at the edge of the domain
        
    Returns:
    -------
    float
        the energy of the bound state associated with the stability problem
    (optional) numpy array
        if return_bound_state is set to True, the solution with the required
        energy is also returned
    
    """
    # Define the integration range if none is given.
    if r is None: r = np.linspace(0, 8, 10000)
    # Find the initial gradient of the kink for this wormhole radius.
    ig = find_initial_grad(a, r)
    # Set the initial conditions to search for either even or odd bound states.
    if even:
        ic = [0, ig, 1, 0]
    elif not even:
        ic = [0, ig, 0, 1]
    # Set the range of energies to consider and try the first one.
    Emin = float(0)
    Emax = float(4)
    E = (Emin + Emax)/2
    v = odeint(schrodinger, ic, r, (a, E))
    # Extract the value of the Schrodinger solution at the edge of the domain.
    delta = v[-1,2]

    # Employ a bisection method to find the required energy.
    while (np.abs(delta) > 0.000001 or delta < 0) and E < 4:
        
        if delta > 0:    # Current value of E too small.
            Emin = E     # Only consider larger values of E in future.
        elif delta < 0:  # Current value of E too large.
            Emax = E     # Only consider smaller values of E in future.
            
        # Try new value of E and update the value of the Schrodinger solution
        # at the edge of the domain.
        E = (Emin + Emax)/2
        v = odeint(schrodinger, ic, r, (a, E))
        delta = v[-1,2]
    
    # Return the final value of E with the bound state if requested.
    if return_bound_state:
        return(E, v[:,2])
    else:
        return(E)
        
def int_potential(i, r, a):
    """
    The vector i contains the value of the Phi field, its derivative and the
    integral (from r=0) of the Schrodinger potential corresponding to the kink
    at some value of r. This function takes i to its derivate with respect to r
    at that value of r, given the wormhole radius.
    
    Arguments:
    ---------
    i: list of three floats
        the value of the i vector as described above 
    r: float
        the radius at which the i vector takes this value
    a: float
        the wormhole radius
    
    Returns:
    -------
    list of three floats
        the gradient of the i vector at radius r
    
    """
    # Extract Phi and its gradient from the i vector
    u = i[0]
    w = i[1]
    # Calculate and return the gradient of each component according to the
    # static ODE and the value of the QM potential V at that point.
    dudr = w
    dwdr = - 2*u*(1-u*u) - 2*r*w/(r*r+a*a)
    V = a*a/(r*r+a*a)**2 - 2*(1-3*u*u)
    dIdr = V - 4
    return [dudr, dwdr, dIdr]