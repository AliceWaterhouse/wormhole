import numpy as np
import matplotlib.pyplot as plt
import plotting_functions as pf

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']

r = np.linspace(0, 8, 10000)  # Initialise domain.
r_all = np.concatenate((np.flipud(-r), r))  # Extend to negative values of r.

V0 = -2*(1-3*np.tanh(r_all)**2)
plt.plot(r_all, V0)

# Centre the axes on (0,0), remove the axis ticks at (0,0) and add the legend.
ax = plt.gca()
pf.put_axes_at_zero(ax)
yticks = ax.yaxis.get_major_ticks()
yticks[1].label1.set_visible(False)
xticks = ax.xaxis.get_major_ticks()
xticks[4].label1.set_visible(False)
plt.legend()

# Set axis limits, add labels and save.
plt.axis([-4.0, 4.0, -2.5, 5.0])
plt.xlabel('$r$', fontsize=14)
plt.ylabel('$V_0(r)$', fontsize=14, rotation=0)
ax.xaxis.set_label_coords(0.97, 0.25)
ax.yaxis.set_label_coords(0.4, 0.95)
plt.savefig("flatQMpotential.pdf")