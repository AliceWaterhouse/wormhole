# -*- coding: utf-8 -*-
"""
This script plots the QM potential associated with linear stability analysis of
the a=1 kink, as well as a square well approximation to this potential.

It uses numpy, matplotlib and the local module static.py

"""

import numpy as np
import matplotlib.pyplot as plt


import static as st

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']


# Initialise domain and extend to negative r.
steps = 10000
r = np.linspace(0, 8, steps)  
r_all = np.concatenate((np.flipud(-r),r))

a = 1  # Set wormhole radius.
(ig, phi) = st.find_initial_grad(a, r, returnphi=True)  # Generate the kink.

# Define the QM potential and extend to negative r.
W = a**2/(r**2 + a**2)**2 - 2*(1 - 3*phi[:]**2)
W_all = np.concatenate((np.flipud(W), W))  

# Define the square well approximation.      
square = np.zeros(2*steps)
for i in range(int(steps*0.9)):
    square[i] = 4
for i in range(int(steps*0.9), int(steps*1.1)):
    square[i] = -1
for i in range(int(steps*1.1), steps*2):
    square[i] = 4

# Create a plot showing both the potential and its approximation.
plt.figure(1)
plt.plot(r_all, W_all, 'b')
plt.plot(r_all, square, 'r')
plt.xlabel('$r$', fontsize=14)
plt.ylabel('$V(r)$', fontsize=14, rotation=0)
ax = plt.gca()
ax.xaxis.set_label_coords(0.98, -0.05)
ax.yaxis.set_label_coords(-0.08, 0.95)
plt.savefig('QMpotential.pdf')











