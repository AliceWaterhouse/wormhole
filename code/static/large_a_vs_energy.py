# -*- coding: utf-8 -*-
"""
This script plots the energy of internal modes of the kink against the inverse
square of the wormhole radius. It is designed for checking the relationship
between these quantities for large wormhole radii. It is easy to change whether
to consider even or odd internal modes - the assignment is made in line 21.

It uses numpy, matplotlib and the local module static.py.

"""

import numpy as np

import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerLine2D
from matplotlib.ticker import ScalarFormatter

import static as st

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']


even = False # Plot energies of the even internal mode.

r = np.linspace(0, 8.0, 10000)  # Initialise domain.
# Create arrays to contain the values of 1/a^2 and of the mode's energy.
epsilon_array = np.linspace(0.0000001, 0.01, 20)
E_array = []

# For each value of 1/a^2, find the internal mode energy for the corresponding
# a.
for epsilon in epsilon_array:
    a = 1/np.sqrt(epsilon)
    E = st.find_bound_state_energy(a, r, even=even)    
    E_array.append(E)  # Add this bound state energy to its array.

# Plot the resulting internal mode energies against 1/a^2.    
plt.figure(1)
red_dots, = plt.plot(epsilon_array, E_array, 'ro', label='numerical')

# Add the predicted internal mode energies based on theory. The prediction
# depends on whether we are considering even or odd modes.
x = np.linspace(0, 0.01, 10)
if even:
    even_label = 'predicted: $\omega^2=2\epsilon^2$'
    blue_line, = plt.plot(x, 2*x, 'b', label=even_label)                         
else:
    odd_label = 'predicted: $\omega^2=3+(\pi^2-7)\epsilon^2$'
    blue_line, = plt.plot(x, 3 + (np.pi**2 - 7)*x, 'b', label=odd_label)
    
# Set the axis limits according to whether we are considering even or odd
# modes.
if even:
    plt.axis([0.0, 0.01, 0.0, 0.02])
else:
    plt.axis([0.0, 0.01, 2.99, 3.03])
    ax = plt.gca()
    ax.yaxis.set_major_formatter(ScalarFormatter(useOffset=False))
plt.locator_params(axis='y', nbins=5)
# Label the axes and add the legend.
plt.xlabel('$\epsilon^2$', fontsize=14)
plt.ylabel('$\omega^2$', fontsize=14, rotation=0)
ax = plt.gca()
ax.xaxis.set_label_coords(0.98, -0.07)
ax.yaxis.set_label_coords(-0.12, 0.9)
plt.legend(handler_map={red_dots: HandlerLine2D(numpoints=1)}, loc=4)
# Save the figure, with a different name depending on whether it plots even or
# odd internal modes.
if even: plt.savefig('xi_zero_mode.pdf')
else: plt.savefig('xi_other_mode.pdf')











