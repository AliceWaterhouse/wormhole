# -*- coding: utf-8 -*-
"""
This script determines the kink for a given wormhole radius and plots a
solution to the quantum mechanics problem associated with its stability for a
chosen energy.

It requires numpy, scipy, matplotlib and the local module static.py.

"""

import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

import static as st

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']


a = 0.5                          # Set wormhole radius a.
r = np.linspace(0, 8, 10000)      # Initialise domain.
ig = st.find_initial_grad(a, r)   # Find initial gradient of kink for this a.

E = 4*st.find_bound_state_energy(a,r)              # Set energy in the quantum mechanics problem.
ic = [0, ig, 0, 3 ]   # Set initial conditions for an odd parity solution.
v = odeint(st.schrodinger, ic, r, (a, E))  # Integrate the QM problem.

# Extend the domain to include negative values of r.        
r_all = np.concatenate((np.flipud(-r), r))         
v_all = np.concatenate((-np.flipud(v), v))

# Plot the solution to the QM problem against r.
plt.figure(0)
plt.plot(r_all, v_all[:,2],'b')

# Add a dashed line at r=0 and label the axes.
plt.axvline(x=0, color='k',dashes=[10, 3, 10, 3])
plt.xlabel('$r$')
plt.ylabel('$v(r)$')










