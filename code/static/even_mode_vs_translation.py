# -*- coding: utf-8 -*-
"""
Created on Thu Jun  6 14:15:59 2019

@author: aw592
"""

import static as st
import numpy as np
from matplotlib import pyplot as plt

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']

a = 0.5
r = np.linspace(0, 8, 10000)

ig, static_kink = st.find_initial_grad(a, r, returnphi=True)
E, bs = st.find_bound_state_energy(a, r, return_bound_state=True)
internal_mode = 0.1*bs*(r*r + a**2)**(-1.0/2.0)

plt.figure(0)
plt.plot(r, static_kink, 'b', r, static_kink + internal_mode, 'r')

plt.figure(1)
plt.plot(r, internal_mode, 'r')
plt.plot(r, bs, 'b')