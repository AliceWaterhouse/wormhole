# -*- coding: utf-8 -*-

"""
This script plots the Phi^4 potential which we study on the wormhole. The
constants are chosen such that the minima are zeros of the potential.

The file requires numpy, matplotlib and plotting_functions.

"""

import numpy as np
from matplotlib import pyplot as plt

import plotting_functions as pf

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']

phi = np.linspace(-1.5, 1.5, 100)  # Define domain in which to plot potential.
def U(r):  # Define potential function.
    return (1 - phi**2)**2/2
    
# Create a figure with the axes centred on (0,0).
fig = plt.figure(0)
ax = plt.gca()
pf.put_axes_at_zero(ax)

# Plot the potential, add axis labels in a sensible position, remove the zero
# tick on the y axis and save.
plt.plot(phi, U(phi))
plt.xlabel('$\phi$', fontsize=14)
plt.ylabel('$U(\phi)$', rotation=0, fontsize=14)
ax.xaxis.set_label_coords(0.95, -0.08)
ax.yaxis.set_label_coords(0.38, 0.95)
yticks = ax.yaxis.get_major_ticks() 
yticks[0].label1.set_visible(False)
plt.savefig('potential.pdf')