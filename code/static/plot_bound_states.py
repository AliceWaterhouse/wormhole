# -*- coding: utf-8 -*-
"""
This script finds and plots the bound states of the quantum mechanics problem
associated with stability of the kink. Bound states are those with energy
between 0 and 4.

It uses numpy, matplotlib and the local modules static and plotting_functions.

"""

import numpy as np
import matplotlib.pyplot as plt

import static as st
import plotting_functions as pf

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']


a = 1.0  # Set the wormhole radius.
r = np.linspace(0, 8, 10000)  # Intialise the domain.
r_all = np.concatenate((np.flipud(-r), r))  # Extend to negative values of r.

# Find the bound states.
(E0,v0) = st.find_bound_state_energy(a, r, even=True, return_bound_state=True)
(E1,v1) = st.find_bound_state_energy(a, r, even=False, return_bound_state=True)

if E0 < 4:  # If there is an even bound state...
    v0_all = np.concatenate((np.flipud(v0), v0))  # ...extend it to negative r.
    # Plot the state, centering the axes at (0,0).
    fig = plt.figure(0)
    ax = fig.add_subplot(1, 1, 1)
    pf.put_axes_at_zero(ax)
    plt.plot(r_all, v0_all, 'b',
             label=('$\omega^2$ = '+'{:.6f}'.format(E0)))
    if E1 < 4:  # If there is also an odd bound state  
        v1_all = np.concatenate((np.flipud(-v1), v1))  # Extend to negative r.
        plt.plot(r_all, v1_all, 'r',                   # Add it to the plot.
                 label=('$\omega^2$ = '+'{:.6f}'.format(E1)))
        pf.remove_centre_ticks(ax)  # Remove the axis ticks at x=0 and y=0.
    elif E1 >= 4:
        # Remove the y=0 axis tick.
        yticks = ax.yaxis.get_major_ticks() 
        yticks[0].label1.set_visible(False)
    
    #Label the axes and add the legend.
    plt.xlabel('$r$', fontsize=14)
    plt.ylabel('$v(r)$', fontsize=14, rotation=0)
    plt.legend()
    # Position the axis labels depending on whether an odd state is present.
    if E1 < 4: ax.xaxis.set_label_coords(0.95, 0.28)
    else: ax.xaxis.set_label_coords(0.95, -0.08)
    ax.yaxis.set_label_coords(0.38, 0.95)
    # Save the figure, with file name depending on number of bound states.
    #if E1 < 4: plt.savefig('bound_states.pdf')
    #else: plt.savefig('bound_state.pdf')
elif E0 >= 4:  # If there are no bound states:
    print('$a$ is too small - there are no bound states')







