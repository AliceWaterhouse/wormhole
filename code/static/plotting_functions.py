# -*- coding: utf-8 -*-
"""
plotting
========

This module contains functions that change the axes settings for matplotlib
plots. It requires matplotlib.

Functions
---------
put_axes_at_zero
    Move the axes to pass through (0,0).
remove_centre_ticks
    Remove the axis ticks at the centre of the plot.

"""
def put_axes_at_zero(ax):
    """
    Move left and bottom axes to pass through (0,0), and remove right and top
    axes.
    
    Arguments:
    ---------
    ax: matplotlib subplot
        the subplot in which to move the axes
        
    Side Effects:
    ------------
        Moves the axes.
        
    """
    # Move left and bottom axes to pass through (0,0)    
    ax.spines['left'].set_position('zero')
    ax.spines['bottom'].set_position('zero')
    # Eliminate upper and right axes.
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    # Show ticks in the left and lower axes only
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    
def remove_centre_ticks(ax):
    """Remove the central axis ticks."""
    # Get major y ticks and set the one at the centre to invisible.
    yticks = ax.yaxis.get_major_ticks()
    yticks[(len(yticks) - 1) / 2].label1.set_visible(False)
    # Do the same for the x ticks.
    xticks = ax.xaxis.get_major_ticks()
    xticks[(len(xticks) - 1) / 2].label1.set_visible(False)
