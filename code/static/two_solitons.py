# -*- coding: utf-8 -*-

"""
This script plots the kink for a=0.5 compared with the kink in the limit of large
wormhole radius. The former it generates using the static.py module.

It requires numpy and matplotlib in addition to the static.py and
plotting_functions.py modules.

"""

import numpy as np
import matplotlib.pyplot as plt

import static as st
import plotting_functions as pf

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']

r = np.linspace(0, 8, 10000)  # Initialise domain.
a = 0.5                       # Assign wormhole radius.

# Generate the kink for this wormhole radius.
(ig, phi) = st.find_initial_grad(a=a, r=r, returnphi=True)

# Create a figure with the y axis in the centre of the plot.
fig = plt.figure(0)
ax = fig.add_subplot(1, 1, 1)
pf.put_axes_at_zero(ax)

# Plot the kink against the r array.
plt.figure(0)
plt.plot(r, phi[:], 'b', label='$a=0.5$')
plt.plot(-r, -phi[:], 'b')
# Add the kink in the large a limit and a legend.
plt.plot(r, np.tanh(r), 'r', label='$a=\infty$')
plt.plot(-r, np.tanh(-r), 'r')
plt.legend(loc=4)
# Add axis labels in a sensible position.
plt.xlabel('$r$', fontsize=14)
plt.ylabel('$\Phi(r)$', fontsize=14, rotation=0)
ax.xaxis.set_label_coords(0.97, 0.45)
ax.yaxis.set_label_coords(0.38, 0.95)
# Remove the zero ticks, set the axis limits and save.
pf.remove_centre_ticks(ax)
plt.axis([-4, 4, -1, 1])
plt.savefig('two_solitons.pdf')
