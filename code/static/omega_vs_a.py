# -*- coding: utf-8 -*-

"""
This script plots the even and odd discrete mode frequencies of the wormhole
kink against the wormhole radius.

It requires numpy, matplotlib and the local modules static.py and
plotting_functions.py.

"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter

import static as st


plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']



r = np.linspace(0, 8, 10000)  # Initialise domain.
a_max = 4.0
a_list = np.linspace(0.3, a_max, 500)  # Wormhole radii to consider
even_omega_list = np.array([])
odd_omega_list = np.array([])
omega_max = 2.0

for a in a_list:
    E = st.find_bound_state_energy(a, r, even=True)
    even_omega_list = np.append(even_omega_list, np.sqrt(E))
    Eodd = st.find_bound_state_energy(a, r, even=False)
    odd_omega_list = np.append(odd_omega_list, np.sqrt(Eodd))
    
   
plt.figure(0)   
plt.plot(a_list, even_omega_list, 'b', label='Even mode')
plt.plot(a_list, odd_omega_list, 'r', label='Odd mode')

# Find where the frequency of the even mod goes below 2/n for n=2,3,4
ax = plt.gca()
#ax.axvline(x=0.3, color='k')
odd_min = a_list[np.min(np.where(odd_omega_list < 2.0))]
ax.axvline(x=odd_min, color='grey', ls='-')
ax.axhline(y=np.sqrt(3), color='grey')

number_of_fracs = 5
fracs = np.zeros((number_of_fracs,2), dtype=float)
yticks = [2]
for i in range(number_of_fracs):
    fracs[i,0] = 2.0/float(i+1)
    fracs[i,1] = a_list[np.min(np.where(even_omega_list < fracs[i,0]))]
    ax.axhline(y=fracs[i,0], xmax=fracs[i,1]/a_max, color='grey', ls='-')
    ax.axvline(x=fracs[i,1], ymax=fracs[i,0]/omega_max, color='grey', ls='-')
    if i > 0: yticks.append('2/{}'.format(str(i+1)))
    
yticks.append('$\sqrt{3}$')

plt.xticks((np.append(odd_min, fracs[:,1])).tolist())
ax.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
plt.yticks(np.append(fracs[:,0],np.sqrt(3)))
ax.set_yticklabels(yticks)

#plt.legend(loc=3)

# Set axis limits, add labels and save.
plt.axis([0.0, a_max, 0.0, omega_max])
plt.xlabel('$a$', fontsize=14)
plt.ylabel('$\omega$', fontsize=14, rotation=0)
ax.xaxis.set_label_coords(0.97, -0.05)
ax.yaxis.set_label_coords(-0.08, 0.9)
plt.savefig('omega_vs_a.pdf')










