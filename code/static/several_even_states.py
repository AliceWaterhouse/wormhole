# -*- coding: utf-8 -*-

"""
This script generates and plots a series discrete modes for kinks on wormholes
and also the discrete mode of the flat \phi^4 kink on \R^{1,1}.

It requires numpy, matplotlib and the local modules static.py and
plotting_functions.py.

"""

import numpy as np
import matplotlib.pyplot as plt

import static as st
import plotting_functions as pf

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']


r = np.linspace(0, 8, 10000)  # Initialise domain.
r_all = np.concatenate((np.flipud(-r), r))  # Extend to negative values of r.
a_list = [0.5, 0.4]  # Wormhole radii to consider

# Plot the flat state for comparison.
plt.figure(0)
plt.plot(r_all, 1/np.cosh(r_all)**2, label='$2\sqrt{3}v_0(r)/3$')

for a in a_list:
    (E, v) = st.find_bound_state_energy(a, r, even=True,
                                         return_bound_state=True)
    v_all = np.concatenate((np.flipud(v), v))  # Extend to negative r.
    
    # Plot the state, including the wormhole radius in the legend.
    plt.plot(r_all, v_all, label='$a = $'+str(a))



# Centre the axes on (0,0), remove the axis ticks at (0,0) and add the legend.
ax = plt.gca()
pf.put_axes_at_zero(ax)
yticks = ax.yaxis.get_major_ticks()
yticks[1].label1.set_visible(False)
xticks = ax.xaxis.get_major_ticks()
xticks[4].label1.set_visible(False)
plt.legend(loc=3)

# Set axis limits, add labels and save.
plt.axis([-4.0, 4.0, -1.2, 1.2])
plt.xlabel('$r$', fontsize=14)
plt.ylabel('$\Phi(r)$', fontsize=14, rotation=0)
ax.xaxis.set_label_coords(0.97, 0.45)
ax.yaxis.set_label_coords(0.38, 0.95)
plt.savefig('several_even_modes.pdf')










