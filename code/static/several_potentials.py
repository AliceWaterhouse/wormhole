# -*- coding: utf-8 -*-

"""
This script generates and plots a series of potentials for the quantum
mechanics problem associated with the linear stability analysis of the kink.
Potentials are plotted for wormhole radii 10, 0.5, 0.4, 0.3 and also 1/sqrt(2),
since this is the special value for which the potential vanishes at r=0.

It requires numpy, matplotlib and the local modules static.py and
plotting_functions.py.

"""

import numpy as np
import matplotlib.pyplot as plt

import static as st
import plotting_functions as pf

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']


r = np.linspace(0, 8, 10000)  # Initialise domain.
r_all = np.concatenate((np.flipud(-r), r))  # Extend to negative values of r.
a_list = [10, 1/np.sqrt(2), 0.5, 0.4, 0.3]  # Wormhole radii to consider

for a in a_list:
    (ig, phi) = st.find_initial_grad(a, r, returnphi=True)   # Generate kink.
    W = a**2/(r**2 + a**2)**2 - 2*(1 - 3*phi**2)  # Define QM potential.
    W_all = np.concatenate((np.flipud(W), W))  # Extend to negative r.
    
    # Plot the QM potential, including the wormhole radius in the legend for
    # the maximum and minimum value and the special value of 1/sqrt(2).
    plt.figure(0)
    if a == 1/np.sqrt(2):
        plt.plot(r_all, W_all, label='$a = 1/\sqrt{2}$')
    else:
        plt.plot(r_all, W_all, label='$a = $'+str(a))


# Centre the axes on (0,0), remove the axis ticks at (0,0) and add the legend.
ax = plt.gca()
pf.put_axes_at_zero(ax)
yticks = ax.yaxis.get_major_ticks()
yticks[1].label1.set_visible(False)
xticks = ax.xaxis.get_major_ticks()
xticks[4].label1.set_visible(False)
plt.legend()

# Set axis limits, add labels and save.
plt.axis([-2.0, 2.0, -2.0, 10.0])
plt.xlabel('$r$', fontsize=14)
plt.ylabel('$V(r)$', fontsize=14, rotation=0)
ax.xaxis.set_label_coords(0.97, 0.1)
ax.yaxis.set_label_coords(0.38, 0.95)
#plt.savefig('several_potentials.pdf')










