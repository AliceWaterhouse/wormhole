# -*- coding: utf-8 -*-

"""
This script integrates the static equation for \Phi^4 theory on the wormhole
with the wormhole radius and gradient of the field at r=0 specified. It assumes
that the field value is zero at r=0. It plots the solution as a function of r
for positive values of r.

It requires numpy, scipy, matplotlib and the local module static.py.

"""

import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

import static as st

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']


# Initialise domain.
r = np.linspace(0, 8, 10000)

# Specify wormhole radius and gradient of field at r=0.
a = 0.3
ig = 2.821815212

# Integrate the static equation for these parameters in this domain.
phi = odeint(st.static_model,[0,ig],r,(a,))

# Plot the kink against the r array, giving the initial gradient in the legend.
plt.figure(0)
plt.plot(r,phi[:,0])
plt.legend(['$\Phi^\prime(0) =$ ' + "{:.6f}".format(ig)], loc=4)

# Add axis labels.
plt.xlabel('$r$')
plt.ylabel('$\Phi(r)$')












