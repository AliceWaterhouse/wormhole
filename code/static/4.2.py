# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 15:17:56 2019

@author: aw592
"""

import static as st
import numpy as np
from scipy.integrate import odeint
from matplotlib import pyplot as plt

plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}']

a = 0.5
r = np.linspace(0,8,10000)


ig,Phi = st.find_initial_grad(a, r, returnphi=True)

cos_ic = [0, ig, 0.88, 0.88]
sin_arg = -0.035*np.pi
sin_ic = [0, ig, 2.85*np.sin(sin_arg), 2.85*np.cos(sin_arg)]

E,v = st.find_bound_state_energy(a, r, return_bound_state=True)
v_sin = odeint(st.schrodinger, sin_ic, r, (a, 4*E))
v_cos = odeint(st.schrodinger, cos_ic, r, (a, 4*E))
r_all = np.append(-np.flipud(r),r)
plt.figure(0)
plt.plot(r, v_sin[:,2])
plt.plot(r, np.sin(np.sqrt(4*(E-1))*(r)), 'r')


plt.figure(1)
plt.plot(r, v_cos[:,2])
plt.plot(r, np.cos(np.sqrt(4*(E-1))*(r)), 'r')

f20 = -3*v*v*Phi/(2*np.sqrt(r**2+a**2))
F_all = np.append(-np.flipud(f20),f20)
plt.figure(2)
plt.plot(r_all,F_all)

in_prod = (2*np.dot(f20,v_sin[:,2])*8.0/10000.0)**2 + (2*np.dot(f20,v_cos[:,2])*8.0/10000.0)**2
Gamma = 4/(np.sqrt(4*E - 4)*np.sqrt(E)) * in_prod
const = (a*Gamma)**(-1.0/2.0)
print(str(const))



