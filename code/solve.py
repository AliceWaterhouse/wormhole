# -*- coding: utf-8 -*-
"""
Created on Fri May 17 12:03:47 2019

@author: aw592
"""

import numpy as np
from scipy.fftpack import fft
import matplotlib.pyplot as plt
from static import static as st
from matplotlib import animation
import time
import dynamic2 as dyn

start = time.time()

    
a = 0.5
h = 0.01
max_time = 10.0
space_points = 2*int(np.floor(np.pi/(2.0*h))) + 1
middle = int(np.floor(space_points/2))
Y = np.linspace(-np.pi/2, np.pi/2, space_points)
alpha = np.zeros((space_points, 4))
alpha[:,0] = - np.sin(Y)
alpha[:,1] = - np.cos(Y)
alpha[:,2] = a*a/(np.cos(Y)*np.cos(Y))
alpha[:,3] = 2*np.tan(Y)

M = np.array([[np.zeros(space_points), alpha[:,0], np.ones(space_points), np.zeros(space_points), np.zeros(space_points), np.zeros(space_points)],
              [np.zeros(space_points), alpha[:,1]-0.1*np.ones(space_points), np.zeros(space_points), 0.1*np.ones(space_points), alpha[:,0], np.ones(space_points)],
              [2*alpha[:,2], alpha[:,3], alpha[:,1]+alpha[:,0]*alpha[:,3], np.zeros(space_points), np.ones(space_points), alpha[:,0]]])

FD4 = np.zeros((space_points, space_points))
FD4[0,:5] = np.array([-25, 48, -36, 16, -3], dtype=float)/(12*h)
FD4[1,:5] = np.array([-3, -10, 18, -6, 1], dtype=float)/(12*h)
for i in range(2, space_points-2):
    FD4[i,i-2:i+3] = np.array([1, -8, 0, 8, -1], dtype=float)/(12*h)
FD4[-2,-5:] = np.array([-1, 6, -18, 10, 3], dtype=float)/(12*h)
FD4[-1,-5:] = np.array([3, -16, 36, -48, 25], dtype=float)/(12*h)

KO = np.zeros((space_points, space_points))
KO[0,:4] = np.array([-20, 15, -6, 1], dtype=float)
KO[1,:5] = np.array([15, -20, 15, -6, 1], dtype=float)
KO[2,:6] = np.array([-6, 15, -20, 15, -6, 1], dtype=float)
for i in range(3, space_points-3):
    KO[i,i-3:i+4] = np.array([1, -6, 15, -20, 15, -6, 1], dtype=float)
KO[-3,-6:] = np.array([1, -6, 15, -20, 15, -6], dtype=float)
KO[-2,-5:] = np.array([1, -6, 15, -20, 15], dtype=float)
KO[-1,-4:] = np.array([1, -6, 15, -20], dtype=float)

r = a*np.tan(Y)
r = r[middle:-1]
for i in range(r.size):
    if r[i] > 8.0:
        r = r[0:i-1]
        break        
ig, static_kink = st.find_initial_grad(a, r, returnphi=True)
ic_pos = np.append(static_kink, np.ones(int(np.ceil(space_points/2))+1 - r.size))
initial_condition = np.append(-np.flipud(ic_pos)[0:-1], ic_pos)

(E, bound_state_r) = st.find_bound_state_energy(a, r, return_bound_state=True)
bound_state_ext = np.append(bound_state_r, np.zeros(int(np.ceil(space_points/2))+1 - r.size))
bound_state = np.append(np.flipud(bound_state_ext)[0:-1], bound_state_ext)

F = np.zeros((Y.size, 3))
F[:,0] = initial_condition + np.exp(-np.tan(Y)*np.tan(Y))
dFdy = np.zeros(F.shape)
dFdy = dyn.update_spatial_derivative(F, dFdy, h)
F[:,1] = dFdy[:,0]
F[:,2] = dFdy[:,0]*np.sin(Y)

F_intermediate = np.zeros(F.shape)
Fdot1 = np.zeros(F.shape)
Fdot2 = np.zeros(F.shape)
Fdot3 = np.zeros(F.shape)
Fdot4 = np.zeros(F.shape)
step = h/2.0
peaks = np.array([])
peak_times = np.array([])

time_points = int(np.floor(max_time/step))
s = np.linspace(0, max_time, time_points)
Phi = np.zeros((space_points,time_points,3))
#dPhi = np.zeros((space_points, time_points,3))
for s_index in range(time_points):
    Phi[:,s_index,:] = F
    F = dyn.rk_step(F, dFdy, alpha, a, h, step, F_intermediate, Fdot1, Fdot2, Fdot3,
            Fdot4, FD4, KO)
    if s_index % 100 == 0: print(s_index*step)
    if ((Phi[middle,s_index,2]
        *Phi[middle,s_index-1,2] < 0)
        ):
        peaks = np.append(peaks, np.abs(F[middle,0]))
        peak_times = np.append(peak_times, s[s_index])


end = time.time()
        
plt.figure(0)
#plt.plot(Y, Phi[:,0,0],'b')
#plt.plot(Y, Phi[:,-1,0],'r')
plt.plot(Y, initial_condition, 'g')

#plt.figure(1)
#plt.plot(np.linspace(0, max_time, time_points), dPhi[150,:,0])
#plt.plot(np.linspace(0, max_time, time_points), Phi[150,:,1], 'r')

plt.figure(2)
plt.plot(s, np.abs(Phi[middle,:,0]))
#plt.savefig('oscillating_decay.pdf')

plt.figure(3)
plt.loglog(peak_times, peaks, 'b', basey=2)
plt.loglog(peak_times, 5.0*peak_times**(-1.0/2.0), 'k', basey=2)

peak_data = np.transpose(np.array([peak_times, peaks]))
filename = "{}_{}_{}.npy".format(str(a), str(h), str(max_time))
np.save(filename, peak_data)

plt.figure(4)
ft = fft(Phi[middle,:,0])
plt.plot(np.linspace(0, time_points, time_points)*2*np.pi/(a*time_points*step),
         np.abs(ft), 'r')
plt.axvline(x=np.sqrt(st.find_bound_state_energy(a)), color='k')
plt.axis([0.0, 6.0, 0.0, 5000.0])



#periods = np.zeros(peak_times.size - 1)  
#for osc in range(peak_times.size - 1):
#    periods[osc] = peak_times[osc + 1] - peak_times[osc]


fig = plt.figure(5)
ax = plt.axes(xlim=(-np.pi/2,np.pi/2),ylim=(-1.2,1.2))
line, = ax.plot([],[],'b',lw=2)
static, = ax.plot([],[],'r',lw=2)


def animate(j):
    line.set_data(Y, Phi[:,10*j,0])
    static.set_data(Y, bound_state)
    return line,
    
anim = animation.FuncAnimation(fig, animate, frames=min(5000, int(np.floor(time_points/10))),
                               interval=1)

anim.save('animation.mp4', fps=250, bitrate=1000)
print("Time = " + str(end-start) + " seconds")
print("Time = " + str((end-start)/60) + " minutes")