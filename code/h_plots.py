# -*- coding: utf-8 -*-
"""
Created on Wed Jun  5 12:21:04 2019

@author: aw592
"""
import numpy as np
from matplotlib import pyplot as plt

d05 = np.load("out/peaks_even_0.5_0.05_10000.0.npy")
d005 = np.load("out/peaks_even_0.5_0.005_2000.0.npy")
d0005 = np.load("out/even_0.5_0.0005_10000.0.npy")
d0002 = np.load("out/peaks_even_0.5_0.0002_2000.0.npy")

plt.figure(0)
#plt.loglog(d05[:,0], d05[:,1], label='exptan, $h=0.05$')
plt.loglog(d005[:,0], d005[:,1], 'b', label="exptan; $h=0.005$")
plt.loglog(d0005[:,0], d0005[:,1], 'r', label="exptan; $h=0.0005$")
plt.loglog(d0002[:,0], d0002[:,1], 'g', label="exptan; $h=0.0002$")
#plt.loglog(d0005[:,0], 4.1*d0005[:,0]**(-1.0/2.0), 'k', label='$4.1s^{-1/2}$')
plt.axis([1.0,10000.0,0.05,2.0])
plt.legend(loc=3)
plt.xlabel('$s$')
plt.ylabel('$\phi(0,s)$ at extrema')
#plt.savefig('plots/exptan_h_comparison.pdf')

