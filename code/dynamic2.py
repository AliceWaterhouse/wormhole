# -*- coding: utf-8 -*-

"""
dynamic
=======

Provides the functions required to integrate the full PDE describing the
dynamics of the Phi^4 theory on the wormhole spacetime. The PDE will be solved
by the method of lines in terms of the function Phi=F[0] and two auxilliary
variables Psi=F[1] and Pi=F[2]. Note that the choice of independent variables
is such that y represents space and s represents time.

The module requires numpy.

Functions
---------
update_spatial_derivative(F, dFdy, h)
    Calculate and return the gradient of F wrt y based on the current values of
    F at an array of values of y, using the method of finite differences.
update_time_derivative(F, dFdy, dFds, Y, a, h)
    Calculate and return the gradient of F wrt s based on the current values of
    F and dFdy at an array of values of y, based on the full dynamical PDE.
rk_step(F, dFdy, Y, a, h, step, f_intermediate, fdot1, fdot2, fdot3,
        fdot4)
    Advance the values in F by one time step according to the fourth order
    Runge-Kutta method, and return the advanced values.
    
"""

import numpy as np


def update_spatial_derivative(F, dFdy, h):
    """
    Use fourth order finite differences to calculate the gradient of F with
    respect to y, given the current values of F at an array of points in the
    spatial domain.
    
    Arguments:
    ---------
    F: numpy array with three columns
        the value of Phi, Psi and Pi at each point y in the spatial domain
    dFdy: numpy array with three columns
        the gradient wrt y of Phi, Psi and Pi at each point y in the spatial
        domain
    h: float
        the distance between points in the spatial domain
        
    Returns:
    -------
    dFdy: numpy array with three columns
        the gradient wrt y of Phi, Psi and Pi at each point y in the spatial
        domain
    
    """
    Fp1 = np.append(F[1:,:], np.zeros((1,3)), 0)
    Fp2 = np.append(F[2:,:], np.zeros((2,3)), 0)
    Fm1 = np.append(np.zeros((1,3)), F[:-1,:], 0)
    Fm2 = np.append(np.zeros((2,3)), F[:-2,:], 0)
    dFdy = (Fm2 - 8*Fm1 + 8*Fp1 - Fp2)/(12*h)
    dFdy[0,:] = (- 25*F[0,:] + 48*F[1,:] - 36*F[2,:]
                         + 16*F[3,:] - 3*F[4,:])/(12*h)
    dFdy[1,:] = (- 3*F[0,:] - 10*F[1,:] + 18*F[2,:]
                         - 6*F[3,:] + F[4,:])/(12*h)
    dFdy[-2,:] = (3*F[-1,:] + 10*F[-2,:] - 18*F[-3,:]
                          + 6*F[-4,:] - F[-5,:])/(12*h)
    dFdy[-1,:] = (25*F[-1,:] - 48*F[-2,:] + 36*F[-3,:]
                          - 16*F[-4,:] + 3*F[-5,:])/(12*h)
    return dFdy
    
    
def update_spatial_derivative1(F, dFdy, h):
    for funct in range(3):  # For each function (column) in F:
        # Calculate the derivative of that column using standard first order
        # finite difference approximations. The forward and backward difference
        # formulae are applied for the first and last points in the spatial
        # domain, whilst the central difference formula is used for all the
        # points in between.
        dFdy[0,funct] = (- 25*F[0,funct] + 48*F[1,funct] - 36*F[2,funct]
                         + 16*F[3,funct] - 3*F[4,funct])/(12*h)
        dFdy[1,funct] = (- 3*F[0,funct] - 10*F[1,funct] + 18*F[2,funct]
                         - 6*F[3,funct] + F[4, funct])/(12*h)
        for y_i in range(2, F[:,0].size-2):
            dFdy[y_i,funct] = (F[y_i-2,funct] - 8*F[y_i-1,funct]
                               + 8*F[y_i+1,funct] - F[y_i+2,funct])/(12*h) 
        dFdy[-2,funct] = (3*F[-1,funct] + 10*F[-2,funct] - 18*F[-3,funct]
                          + 6*F[-4,funct] - F[-5, funct])/(12*h)
        dFdy[-1,funct] = (25*F[-1,funct] - 48*F[-2,funct] + 36*F[-3,funct]
                          - 16*F[-4,funct] + 3*F[-5,funct])/(12*h)
    
    return dFdy
    
    

def update_spatial_derivative_first(F, dFdy, h):
    for funct in range(3):
        dFdy[0,funct] = (F[1,funct] - F[0,funct])/h
        for y_index in range(1, F[:,0].size-1):
            dFdy[y_index,funct] = (F[y_index+1,funct]
                                   - F[y_index-1,funct])/(2*h)                   
        dFdy[-1,funct] = (F[-1,funct] - F[-2,funct])/h
    return dFdy   
      
def update_time_derivative(F, dFdy, dFds, alpha, a, h):
    """
    Return the derivative of F with respect to y based on current values of F
    using the PDE.
    
    Arguments:
    ---------
    F: numpy array with three columns
        the value of Phi, Psi and Pi at each point y in the spatial domain
    dFdy: numpy array with three columns
        the gradient wrt y of Phi, Psi and Pi at each point y in the spatial
        domain
    dFds: numpy array with three columns
        the gradient wrt s of Phi, Psi and Pi at each point y in the spatial 
        domain
    Y: numpy vector
        the vector of points y in the spatial domain
    a: float
        the wormhole radius
        
    Returns:
    -------
    dFds: numpy array with three columns
        the gradient wrt s of Phi, Psi and Pi at each point y in the spatial 
        domain
        
    """
    Fp1 = np.append(F[1:,:], np.zeros((1,3)), 0)
    Fp2 = np.append(F[2:,:], np.zeros((2,3)), 0)
    Fp3 = np.append(F[3:,:], np.zeros((3,3)), 0)
    Fm1 = np.append(np.zeros((1,3)), F[:-1,:], 0)
    Fm2 = np.append(np.zeros((2,3)), F[:-2,:], 0)
    Fm3 = np.append(np.zeros((3,3)), F[:-3,:], 0)
    
    dFdy = (Fm2 - 8*Fm1 + 8*Fp1 - Fp2)/(12*h)
    dFdy[0,:] = (- 25*F[0,:] + 48*F[1,:] - 36*F[2,:]
                         + 16*F[3,:] - 3*F[4,:])/(12*h)
    dFdy[1,:] = (- 3*F[0,:] - 10*F[1,:] + 18*F[2,:]
                         - 6*F[3,:] + F[4,:])/(12*h)
    dFdy[-2,:] = (3*F[-1,:] + 10*F[-2,:] - 18*F[-3,:]
                          + 6*F[-4,:] - F[-5,:])/(12*h)
    dFdy[-1,:] = (25*F[-1,:] - 48*F[-2,:] + 36*F[-3,:]
                          - 16*F[-4,:] + 3*F[-5,:])/(12*h)
    #dFdy = update_spatial_derivative(F, dFdy, h)
    dFds[:,0] = F[:,2] + alpha[:,0]*F[:,1]
    dFds[:,1] = (dFdy[:,2] + alpha[:,1]*F[:,1] + alpha[:,0]*dFdy[:,1]
                 - 0.1*(F[:,1] - dFdy[:,0]))
    dFds[:,2] = (dFdy[:,1] + alpha[:,1]*F[:,2] + alpha[:,0]*dFdy[:,2]
                 + alpha[:,3]*(F[:,1] + alpha[:,0]*F[:,2])
                 + 2*alpha[:,2]*F[:,0]*(1-F[:,0]*F[:,0]))
    
    
    # Kreiss-Oliger dissipation
    #for y_i in range(3, F[:,0].size-3):
    #    dFds[y_i,2] += (F[y_i-3,2] + F[y_i+3,2] - 6*(F[y_i-2,2] + F[y_i+2,2])
    #                    + 15*(F[y_i-1,2] + F[y_i+1,2]) - 20*(F[y_i,2]))
    
    dFds[:,2] += (Fm3[:,2] + Fp3[:,2] - 6*(Fm2[:,2] + Fp2[:,2])
                        + 15*(Fm1[:,2] + Fp1[:,2]) - 20*(F[:,2]))
    dFds[0,:] = 0
    dFds[-1,:] = 0
    
    
    return dFds
    
def update_time_derivative1(F, dFdy, dFds, alpha, a, h, FD4, KO):
    dFdy = np.matmul(FD4, F)
    #dFds = np.matmul(M, np.append(np.transpose(F), np.transpose(dFdy), axis=0))
    dFds[:,0] = F[:,2] + alpha[:,0]*F[:,1]
    dFds[:,1] = (dFdy[:,2] + alpha[:,1]*F[:,1] + alpha[:,0]*dFdy[:,1]
                 - 0.1*(F[:,1] - dFdy[:,0]))
    dFds[:,2] = (dFdy[:,1] + alpha[:,1]*F[:,2] + alpha[:,0]*dFdy[:,2]
                 + alpha[:,3]*(F[:,1] + alpha[:,0]*F[:,2])
                 + 2*alpha[:,2]*F[:,0]*(1-F[:,0]*F[:,0]))
                 
    dFds[:,2] += np.matmul(KO, F[:,2])
    
    dFds[0,:] = 0
    dFds[-1,:] = 0
    return dFds

   
def rk_step(F, dFdy, M, a, h, step, F_intermediate, Fdot1, Fdot2, Fdot3,
            Fdot4, FD4, KO):
    """
    Return the values in F advanced by one time step according to the fourth
    order Runge-Kutta method.
    
    Arguments:
    ---------
    F: numpy array with three columns
        the value of Phi, Psi and Pi at each point y in the spatial domain
    dFdy: numpy array with three columns
        the gradient wrt y of Phi, Psi and Pi at each point y in the spatial
        domain
    Y: numpy vector
        the vectoy of points y in the spatial domain
    a: float
        the wormhole radius
    h: float
        the distance between points in the spatial domain
    step: float
        the size of each time step
    F_intermediate: numpy array with three columns
        estimate of F at time (s + step/2) based on intermediate gradient
        estimate Fdot1, then Fdot2, then Fdot3
    Fdot1: numpy array with three columns
        estimate of dFds at time s based on F at time s
    Fdot2: numpy array with three columns
        estimate of dFds at time (s + step/2) based on Fdot1 and F at time s
    Fdot3: numpy array with three columns
        estimate of dFds at time (s + step/2) based on Fdot2 and F at time s
    Fdot4: numpy array with three columns
        estimate of dFds at time (s + step) based on Fdot3 and F at time s

    """          
    Fdot1 = update_time_derivative(F, dFdy, Fdot1, M, a, h)
    F_intermediate = F + Fdot1*step/2
    
    Fdot2 = update_time_derivative(F_intermediate, dFdy, Fdot2, M, a, h)
    F_intermediate = F + Fdot2*step/2
    
    Fdot3 = update_time_derivative(F_intermediate, dFdy, Fdot3, M, a, h)
    F_intermediate = F + Fdot3*step    
    
    Fdot4 = update_time_derivative(F_intermediate, dFdy, Fdot4, M, a, h)
    return F + (Fdot1 + Fdot2*2 + Fdot3*2 + Fdot4)*step/6
    
