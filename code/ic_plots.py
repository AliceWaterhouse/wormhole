# -*- coding: utf-8 -*-
"""
Created on Wed Jun  5 13:58:49 2019

@author: aw592
"""

import numpy as np
from matplotlib import pyplot as plt

d1 = np.load("out/peaks_even_0.5_0.0002_2000.0.npy")
d2 = np.load("out/peaks_even2.0_0.5_0.0002_2000.0.npy")
d_5 = np.load("out/peaks_even0.5_odd0.0_a0.5_h0.0002_200.0.npy")
d_1 = np.load("out/peaks_even0.1_0.5_0.005_2000.0.npy")
d_01 = np.load("out/peaks_even0.01_0.5_0.005_2000.0.npy")
mix11 = np.load("out/peaks_even1.0_odd1.0_a0.5_h0.0002_2000.0.npy")
bs0005 = np.load("out/0_0.5_0.0005_40000.0.npy")
plt.figure(1)
plt.loglog(d2[:,0], d2[:,1], 'b', label="$2e^{-\mathrm{tan}^2y}$")
plt.loglog(d1[:,0], d1[:,1], 'r', label="$e^{-\mathrm{tan}^2y}$")
plt.loglog(d_5[:,0], d_5[:,1], 'g', label="$0.5e^{-\mathrm{tan}^2y}$")
plt.loglog(mix11[:,0], mix11[:,1], 'c', label="$(1+\mathrm{tanh}y)e^{-\mathrm{tan}^2y}$")
#plt.loglog(bs0005[:,0], bs0005[:,1], 'm', label="bs; $h=0.0005$")
plt.loglog(d1[:,0], 4.2*d1[:,0]**(-1.0/2.0), 'k', label='$4.2s^{-1/2}$')
plt.axis([2.6,2000.0,0.08,3.0])
plt.legend(loc=1)
plt.xlabel('$s$')
plt.ylabel('$\phi(0,s)$ at extrema')
#plt.savefig("plots/ic_comparison_0002.pdf")

#plt.figure(2)
#plt.loglog(d_01[:,0], d_01[:,1], 'c', label="$0.01e^{-\mathrm{tan}^2y}$; $h=0.005$")
#plt.loglog(d_1[:,0], d_1[:,1], 'g', label="$0.1e^{-\mathrm{tan}^2y}$; $h=0.005$")
#plt.legend(loc=3)